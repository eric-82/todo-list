package com.example.student.todo;

public class ToDoItem {
    public String title;
    public String dataAdded;
    public String dataDue;
    public String category;

    public ToDoItem(String title, String dataAdded, String dataDue, String category){
        this.title = title;
        this.dataAdded = dataAdded;
        this.dataDue = dataDue;
        this.category= category;
    }

}
