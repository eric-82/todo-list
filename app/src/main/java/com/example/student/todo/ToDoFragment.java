package com.example.student.todo;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.security.PrivateKey;

public class ToDoFragment extends Fragment  {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private ToDoActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback =  null;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reddit_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        ToDoItem i1 = new ToDoItem("hi", "i", "am", "first");
        ToDoItem i2 = new ToDoItem("hi", "i", "am", "second");
        ToDoItem i3 = new ToDoItem("hi", "i", "am", "third");
        ToDoItem i4 = new ToDoItem("hi", "i", "am", "last");

        activity.ToDoItem.add(i1);
        activity.ToDoItem.add(i2);
        activity.ToDoItem.add(i3);
        activity.ToDoItem.add(i4);

        public void updateUserInterface(){
            ToDoActivity adapter = new ToDoActivity(ActivityCallback);
            recyclerView.setAdapter(adapter);
        }

        return view;
    }
}
